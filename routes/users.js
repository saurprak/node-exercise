var models = require('../models');
var express = require('express');
var router = express.Router();

router.post('/', function (req, res) {
    models.User.findOrCreate({
        where: {username: req.body.username}, defaults: {
            displayName: req.body.displayName,
            department: req.body.department,
        }
    }).then(function (val) {
        if (val[1])
            res.send(val[0]);
        else
            res.sendStatus(409);
    })
});

router.get('/', function (req, res) {
    models.User.all().then(function (data) {
        res.send(data);
    });
});

router.get('/:username', function (req, res) {
    models.User.findOne({where: {username: req.params.username}}).then(function (data) {
        if (data == null)
            res.sendStatus(404);
        else
            res.send(data);
    });
});
router.delete('/:username', function (req, res) {
    models.User.destroy({
        where: {
            username: req.params.username
        }
    }).then(function (val) {
        console.log(val)
        if (val == 0)
            res.sendStatus(404);
        else
            res.send({deleted: true});
    });
});


module.exports = router;
