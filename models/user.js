'use strict';
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('User', {
        username: DataTypes.STRING,
        displayName: DataTypes.STRING,
        department: DataTypes.STRING,
    });

    User.associate = function (models) {

    };

    return User;
};
