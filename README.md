# MathWorks Exercise

## Running the application

### Using Docker pull

- ``docker pull saurprak/mathwork-exercise``
- ``docker run -p 49160:3000 -d saurprak/mathworks-exercise``
- App will be up and running at http://localhost:49160/users 

### Using repository
- Clone or download the node app
- ``npm install``
- ``npm start``
- App will be up and running at http://localhost:3000/users

## Making Requests
The web service will be used to query user data: 

GET /users 

``
curl -i -H "Accept: application/json" -H "Content-Type: application/json"  localhost:49160/users
``
	
GET /users/{username} 

``
curl -i -H "Accept: application/json" -H "Content-Type: application/json"  localhost:49160/users/jbose1
``

POST /users

``
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X POST -d ' { "username":"jbose1", "displayName":"josh", "department":"sales"}'  localhost:49160/users
`` 
DELETE /users/{username} 

``
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X DELETE localhost:49160/users/jbose1
``

## Run tests

- ``npm test``
- Above will run unit and integration test cases.  